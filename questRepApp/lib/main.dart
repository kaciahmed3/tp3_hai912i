import 'package:application2/data/data_providers/data_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:application2/views/screens/homepage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:application2/business_logic/cubits/question_quiz_cubit/questions_quiz_cubit.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());

}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => QuestionsQuizCubit(DataProvider.getAllQuestions(),0,0),
        child: BlocBuilder<QuestionsQuizCubit, QuestionsQuizState >(
            builder: (_, theme) {
              return MaterialApp(
                debugShowCheckedModeBanner: true,
                theme: ThemeData(
                  primarySwatch: Colors.blueGrey,
                ),
                home: HomePage(),
              );
            }
        )
    );

  }
}


