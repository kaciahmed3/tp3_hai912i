import 'package:application2/business_logic/cubits/question_quiz_cubit/questions_quiz_cubit.dart';import 'package:application2/data/models/question.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/src/provider.dart';

class QuestionsRepository {
  static List<Question> questions = List.empty(growable: true);


  static Future<void> getAllQuestions() async {
    questions=[];
    QuerySnapshot query = await FirebaseFirestore.instance.collection('questions').get();
    List<QueryDocumentSnapshot> docs = query.docs;
    for (var doc in docs) {
      if (doc.data() != null) {
        var data = doc.data() as Map<String, dynamic>;
        questions.add(Question(
            question : data['text'], isCorrect : data['is_correct'], thematic : data['thematic']));
      }
    }
  }
  static Future<void> addQuestions(Question question, BuildContext context) {
    return FirebaseFirestore.instance.collection('questions')
        .add({
      'text': question.question,
      'is_correct': question.isCorrect,
      'thematic' : question.thematic
    }).then((value) => context.read<QuestionsQuizCubit>().restart())
        .catchError((error) => print("Failed to add user: $error"));
  }


}