class Question{

  late String question;
  late bool  isCorrect;
  late String thematic;

  Question({required this.question, required this.isCorrect,required this.thematic });

  factory  Question.fromJson(Map<String, dynamic> json) => Question(
    question :json['text'],
    isCorrect : json['is_correct'],
      thematic: json['thematic']
  );

  Map<String, dynamic> toJson() {
    return {
      'question': question,
      'is_correct': isCorrect,
      'thematic': thematic
    };
  }
}