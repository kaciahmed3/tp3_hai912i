import 'package:application2/data/data_providers/data_provider.dart';
import 'package:application2/data/models/question.dart';
import 'package:application2/views/screens/result.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'questions_quiz_state.dart';

class QuestionsQuizCubit extends Cubit<QuestionsQuizState> {

  List<Question> _questions=[] ;
  int _index=0 ;
  int _score=0;

  QuestionsQuizCubit(this._questions,this._index,this._score) : super(QuestionsQuizInitial(_questions,0,0));

  void nextQuestion(BuildContext context) {
    if (index < questions.length - 1) {
      index++;
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                  score: _score
              )));
    }
    emit(QuestionsQuizInitial(questions, index, score));
  }
  void checkAnswer(bool userChoice, BuildContext context){
    if (questions[index].isCorrect==userChoice) {
      _score++ ;
      nextQuestion(context);
    } else {
      _score--;
      nextQuestion(context);
    }
    emit(QuestionsQuizInitial(questions, index, score));
  }
  void restart(){
    index =0;
    score=0;
    _questions = DataProvider.getAllQuestions();
    emit(QuestionsQuizInitial(questions, index, score));
  }

  int get score => _score;

  set score(int value) {
    _score = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  List<Question> get questions => _questions;

  set questions(List<Question> value) {
    _questions = value;
  }

}
